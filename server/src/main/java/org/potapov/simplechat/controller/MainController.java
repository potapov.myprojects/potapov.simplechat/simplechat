package org.potapov.simplechat.controller;

import org.potapov.simplechat.entity.Ping;
import org.potapov.simplechat.repository.PingRepositoryImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/")
public class MainController {
    private PingRepositoryImpl pingRepository;

    public MainController(PingRepositoryImpl pingRepository) {
        this.pingRepository = pingRepository;
    }

    @GetMapping("index")
    String getIndex() {
        return "Hellow, buddy!";
    }

    @GetMapping("res")
    ResponseEntity<Ping> getResponse() {
        List<Ping> list = pingRepository.getAllPing();
        if (CollectionUtils.isEmpty(list)) {
            return ResponseEntity.badRequest().body(null);
        }
        return ResponseEntity.ok().body(list.get(0));
    }

}
