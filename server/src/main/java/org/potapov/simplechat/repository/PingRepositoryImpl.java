package org.potapov.simplechat.repository;

import org.potapov.simplechat.entity.Ping;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class PingRepositoryImpl {
    private String url = "jdbc:mysql://localhost:3306/db";
    private String username = "root";
    private String password = "1";
    private String sql = "select * from ping";

    public List<Ping> getAllPing() {
        List<Ping> list = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(fetch(resultSet));
            }
            connection.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    private Ping fetch(ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;

        Ping ping = new Ping();
        ping.setRequest(resultSet.getString("request"));
        ping.setResponse(resultSet.getString("response"));
        ping.setType(resultSet.getString("type"));

        return ping;
    }
}
